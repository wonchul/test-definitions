#! /bin/sh

set -x

BLACKLISTED_TESTS="-b validate.file.glvideomixer.*"

gst-validate-launcher validate --sync -j2 --no-color --mute $BLACKLISTED_TESTS \
    | ./gst-validate/gst-validate-parser.py

#! /bin/sh

set -x

WORKSPACE=${1}
ASSETS=${2}

#TODO: get the latest gst-integration-testsuites
ASSETS_URL=http://people.collabora.com/~wonchul/gst-integration-testsuites/${ASSETS}.tar.gz

wget --no-check-certificate --progress=dot:giga --directory-prefix=${WORKSPACE} ${ASSETS_URL}
cd ${WORKSPACE}
tar xfm ${ASSETS}.tar.gz

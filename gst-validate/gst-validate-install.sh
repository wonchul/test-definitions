#! /bin/sh

set -x

wget --no-check-certificate --progress=dot:giga ${ARTIFACTS}
tar xfm gstreamer-1.0_armhf.tar.gz
dpkg -i *.deb

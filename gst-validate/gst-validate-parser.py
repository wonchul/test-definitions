#!/usr/bin/env python

import subprocess
import sys
import re

# regex for each interesting part of stdout
result_end_re     = re.compile('\[(\d+) / (\d+)\] (?P<test_case_id>[^:]+): (?P<result>\w+)( (?P<message>.*))?')

# fixup dictionary for translating gst-validate's results into LAVA's
fixupdict = { 'Passed': 'pass', 'Failed': 'fail', 'Timeout': 'unknown' }

def testcase(test_case_id, result):
    subprocess.Popen(["lava-test-case", test_case_id, "--result", result])
    return 0

def main():
    # Parse stdout from gst-validate
    for line in sys.stdin:
        # Output stdout to LAVA's log as well
        sys.stdout.write(line)

        # Locate the results of a test report
        match = result_end_re.match(line)
        if match is not None:
            test_case_id = match.group('test_case_id')

            result = match.group('result')

            # Translate result from gst-validate to LAVA
            if result in fixupdict:
                testcase(test_case_id, fixupdict[result])

if __name__ == '__main__':
    main()
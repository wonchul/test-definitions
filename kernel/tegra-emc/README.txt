Test for the External Memory Controller on Tegra SoCs

This test checks if the system is able to change the rate of the EMC clock.

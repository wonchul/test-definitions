#!/usr/bin/python2.7
#
# Get the kernel to change to each of the frequencies listed in the DT and
# check that the rate change was effective

import os
import sys

clock_dir = '/sys/firmware/devicetree/base/clock@0,60006000'
frequencies = []
ret = 0

for timings_dir in os.listdir(clock_dir):
	if not timings_dir.startswith('emc-timings-'):
		continue
	for file_ in os.listdir(os.path.join(clock_dir, timings_dir)):
		if file_.startswith('timing-'):
			freq = int(file_.split('-')[1])
			frequencies.append(freq)
	break

frequencies.sort()

for freq in frequencies:
	f = os.open('/sys/kernel/debug/emc/rate', os.O_WRONLY)
	os.write(f, str(freq))
	os.close(f)

	f = os.open('/sys/kernel/debug/clk/emc/clk_rate', os.O_RDONLY)
	if int(os.read(f, 1024).strip()) == freq:
		result = 'pass'
	else:
		result = 'fail'
		ret = 1
	os.close(f)

	print '%d-frequency: %s' % (freq, result)

sys.exit(ret)

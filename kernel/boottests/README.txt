Minimal boot tests

Tests in this directory simply boot a board with a specific user-space, but run
no further tests. Useful for minimal smoketesting.

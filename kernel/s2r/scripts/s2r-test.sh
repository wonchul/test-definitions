#!/bin/bash
#
# s2r-test - Test for Suspend-to-RAM
#

TEST_NAME="s2r-test"
RTCPATH="/sys/class/rtc"
DEVS=($(ls ${RTCPATH}))
ret=0

for rtc in ${DEVS[@]}; do
    wakeup=$(cat ${RTCPATH}/${rtc}/device/power/wakeup)
    if [ "${wakeup}" = "enabled" ]; then
        name=$(cat ${RTCPATH}/${rtc}/name)

        echo "Testing S2R and wakeup with ${name}"

        echo "Set ${name} hardware clock"
        if ! hwclock -w -f /dev/rtc0; then
            ret=$?
            echo "Couldn't set ${name} hardware clock (${ret})"
            echo "s2r-test:" "fail"
            exit ${ret}
        fi

        echo +5 > /sys/class/rtc/rtc0/wakealarm && echo mem > /sys/power/state
        echo "s2r-test:" "pass"
        exit ${ret}
    fi
done

echo "No wakeup sources RTC were found"
echo "s2r-test:" "unknown"
exit 1
